---
title: Artikel Spracheditor für PROFFIX
subtitle: Each post also has a subtitle
tags: ["artikel", "proffix","sprachen","übersetzung"]
---


Mit diesem Tool können **Artikelübersetzungen in PROFFIX** sehr einfach und schnell direkt aus einem Excel aktualisiert und erstellt werden. 

Das Tool kann dabei **komplett auf das Excelfile abgestimmt werden**, sodass keine Korrekturen am Excel (Spaltenanpassungen etc.) nötig sind (Standardisierung).

Weiterhin beachtet das Tool dabei die **Standardvorgaben (Laufnr, ErstelltAm, etc.) von PROFFIX.**

Vorlage aus einer beliebigen Excel -Liste:

{{:tools:artikel-spracheditor-proffix-excel.png?nolink|}}

Tool läuft durch und aktualisiert / erstellt automatisch sämtliche Übersetzungen:

{{:tools:artikel-spracheditor-proffix.png?nolink|}}


## Konfiguration

Sämtliche Konfiguration erfolgt über eine **config-spracheditor.json** welche sich im **selben Verzeichnis** wie das Tool befinden muss.


## Beispiel

```json

{
  "database": {
    "database": "DEMODB",
    "hostname": "SQLSRV",
    "parameter": "?connection+timeout=30",
    "password": "1234",
    "port": 1433,
    "username": "sa"
  },
  "fields": {
    "artikelnr": "A",
    "d": {
      "bezeichnung1": "G",
      "bezeichnung2": "R",
      "bezeichnung3": "",
      "bezeichnung4": "",
      "bezeichnung5": "Z"
    },
    "e": {
      "bezeichnung1": "F",
      "bezeichnung2": "",
      "bezeichnung3": "",
      "bezeichnung4": "",
      "bezeichnung5": "Y"
    },
    "f": {
      "bezeichnung1": "H",
      "bezeichnung2": "AE",
      "bezeichnung3": "",
      "bezeichnung4": "",
      "bezeichnung5": "AA"
    },
    "i": {
      "bezeichnung1": "I",
      "bezeichnung2": "AE",
      "bezeichnung3": "",
      "bezeichnung4": "",
      "bezeichnung5": "AB"
    }
  },
  "source": "Artikeltexte.xlsx",
  "worksheet": "Texte"
}
```

## Parameter


## Database



Parameter  |Typ    |Bemerkung |
-----------|-------|-------------------------|
Username   | string  | Benutzername SQL - Server                      |
Password   | string  | Passwort SQL - Server                          |
Database   | string  | PROFFIX Datenbank                              |
Instance   | string  | Wenn vorhanden: Instanz des SQL - Servers      |
Hostname   | string  | Hostname des SQL - Servers; kann auch IP sein  |
Port       | int     | Port des SQL Servers                           |
Parameter  | string  | Div. Parameter für SQL-Server                  |



<WRAP center round tip 100%>
**Single-Sign-On unter Windows** wird unterstützt, d.h. wenn der ausführende Benutzer genügenden administrative Berechtigungen hat um die PROFFIX Datenbank zu bearbeiten kann sowohl Username wie auch Password leer gelassen werden.
</WRAP>


## Fields

Fields sind die Zuweisung des XLSX - Sheets zu den jeweiligen Bezeichnungen in PROFFIX.

Ist also die **englische Bezeichnung 1** für den Artikel jeweils in **Spalte F**, so muss hier lediglich unter e -> Bezeichnung1 der **Buchstabe F** angegeben werden.

Unter Annahme, dass die Excel-Vorlage immer diesselbe ist, muss dieser Konfigurationsschritt jeweils nur einmal erfolgen - alle späteren Aktualsierungen / Erstellung der Übersetzungen sind nur noch ein Knopfdruck.

## Quelle

Parameter  |Typ    |Bemerkung |
-----------|-------|-------------------------|
| Source     | string  | Pfad / Name zum Excelfile                                   |
| Worksheet  | string  | Worksheet des Excelfiles welches die Übersetzungen enthält  |

## Installation

Das Tool besteht aus einer einzigen, ausführbaren Datei und **muss entsprechend nicht installiert sondern einfach ausgeführt werden.**


## Download

Die aktuellste Version kann jeweils von uns bezogen werden. Kontaktieren Sie uns doch für Details.


---
title: Changelog
subtitle: Each post also has a subtitle
tags: ["alltron", "proffix","importer"]
---



## 1.2.2
2018-11-12

### Fixes

- Fix Versionnr. on builing binaries (ec223e7e90d2bdc74186d7d5ee3f7f6dea32c1e5)

## 1.2.1
2018-11-12

### Fixes

- Automatic Versioning (7435fcf906c6020a4400990f28ce4f3a677dfff4)
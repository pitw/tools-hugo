---
title: Alltron-Importer
subtitle: Each post also has a subtitle
tags: ["alltron", "proffix","importer"]
---


Der Alltron - PROFFIX Importer ermöglicht:

  * den **direkten Import spezifischer Alltron Artikel** inkl. **Preis, Detailbeschrieb und Infos** in PROFFIX
  * das **Erstellen von Offerten** in PROFFIX inkl. kompletter Artikel aus Alltron
  * das Erstellen von Offerten in PROFFIX mit Alltron - Artikeln als **freie Position**


{{% button href="https://getgrav.org/" icon="fas fa-download" %}}Get Grav with icon{{% /button %}}


## Features

  * Direktimport / Aktualisierung Artikelstamm in PROFFIX von Alltron.ch
  * Beliebige Dokumenterstellung direkt aus Alltron Warenkorb (inkl. Artikel)
  * Manuelle Suchfunktion von Artikeln Alltron 
  * Import / Aktualisierung komplett über PROFFIX REST-API
  * „Beautifier“ für Bezeichnung 1 - 5 und Texte
  * Alternativ: CSV Export für manuellen Import
  * Keine Installation nötig, direkt ausführbar
  * Konfigurierbar über TUI (Text Based User Interface) oder direkt als JSON


## Konfiguration


{{% notice info %}}
Das Tool verfügt über einen eingebauten Konfigurator, welcher die Konfiguration automatisch durchführt. Diese kann aber manuell übersteuert werden.
{{% /notice %}}


Sämtliche Konfigurationen werden im json - File **config-alltron.json** gespeichert.
Dieses kann auch manuell bearbeitet werden.


## Beispiel

```json
{
  "alltron": {
    "benutzer": "A2SDFDFD",
    "passwort": "123456"
  },
  "lizenz": "...",
  "proffix": {
    "benutzer": "USER",
    "datenbank": "DEMODB",
    "passwort": "03ac43433e15c761ee1a5eb53f0679536234388b4459e13f978d7c846f4",
    "port": "1200",
    "url": "https://rest.demo.com"
  },
  "standard": {
    "artikel": {
      "ertragskonto": 7000,
      "steuercode": 101
    },
    "dokument": {
      "adresse": "1",
      "dokumenttyp": "OF",
      "kondition": "1",
      "lieferart": "POST"
    }
  }
}
```


## Parameter


### Lizenz

Parameter|Typ |Bemerkung
---------|----|----------
| Lizenz     | string  | Erhalten Sie von uns bzw. wird automatisch generiert bei ersten Verwendung (Demo)  |                                         |

### Alltron

Parameter|Typ |Bemerkung
---------|----|----------
| Benutzername  | string  | Benutzername für Alltron.ch  |
| Password      | string  | Passwort für Alltron.ch      |

Ohne diese Angaben funktioniert das Tool trotzdem, es kann aber diverse Angaben (Einkaufspreise etc.) nicht abrufen.

### PROFFIX

Enhält alle Einstellungen für die Verbindung zur PROFFIX REST-API:

Parameter|Typ |Bemerkung
---------|----|----------
| Benutzer   | string  | Benutzername REST-API                       |
| Datenbank  | string  | Datenbankname PROFFIX                       |
| Passwort   | string  | Passwort Benutzer REST-API                  |
| URL        | string  | Adresse REST-API (ohne Port)  |
| Port       | string  | Port REST-API (z.B. 1200)                   |


### Standard

Enthält Standardangaben für den Import. Diese werden beim ersten Start automatisch abgefüllt und können nachträglich im Config - Json geändert werden.

**Artikel**

Parameter|Typ |Bemerkung
---------|----|----------
| Ertragskonto  | int  | Standardertragskonto Artikel  |
| Steuercode    | int  | Standardsteuercode Artikel    |


**Dokument**

Parameter|Typ |Bemerkung
---------|----|----------
| Kondition  | string  | Standardkondition  |
| Lieferart  | string  | Standardlieferart  |

### Download

Die aktuellste Version kann jeweils hier heruntergeladen werden (inkl. kostenloser **Demoversion**):

[[http://update.pitw.ch/go-proffix-alltron-importer/alltron-proffix-importer.zip|Download Alltron Importer für PROFFIX
]]

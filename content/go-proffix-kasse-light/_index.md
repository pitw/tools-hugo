---
title: Kasse Light für PROFFIX
subtitle: Einfache und zuverlässige Kassenlösung mit Barcode Support
tags: ["artikel", "proffix","kasse","barcode"]
---


Mit diesem Tool können **Artikel aus PROFFIX** sehr einfach und schnell mit einem Barcode - Reader gelesen und 
verrechnet werden.



## Konfiguration

Sämtliche Konfiguration erfolgt über eine **config-kasse.json** welche sich im **selben Verzeichnis** wie das Tool befinden muss.


## Beispiel

```json

{
  "proffix": {
    "benutzer": "Gast",
    "datenbank": "DEMODB",
    "passwort": "03ac43433e15c761ee1a5eb53f0679536234388b4459e13f978d7c846f4",
    "port": "1212",
    "url": "https://demo.rest.ch"
  },
  "commands": {
    "send": "KasseSenden",
    "chancel": "KasseAbbrechen",
    "back": "KasseZurueck",
    "delete": "KasseDelete",
    "reset": "KasseReset",
    "inventory": "KasseInventur"
  },
  "log": true,
  "standard": {
    "dokument": {
      "Dokumenttyp": "RG",
      "Konditition": 1,
      "Lieferart": "POST"
    },
    "artikel": {
      "steuercode": 101,
      "steuercode1": 101,
      "ertragskonto": 7000
    }
  }
}
```

## Parameter


### Lizenz

Parameter|Typ |Bemerkung
---------|----|----------
| Lizenz     | string  | Erhalten Sie von uns bzw. wird automatisch generiert bei ersten Verwendung (Demo)  |                                         |


### PROFFIX

Enhält alle Einstellungen für die Verbindung zur PROFFIX REST-API:

Parameter|Typ |Bemerkung
---------|----|----------
| Benutzer   | string  | Benutzername REST-API                       |
| Datenbank  | string  | Datenbankname PROFFIX                       |
| Passwort   | string  | Passwort Benutzer REST-API                  |
| URL        | string  | Adresse REST-API (ohne Port)  |
| Port       | string  | Port REST-API (z.B. 1200)                   |


### Standard

Enthält Standardangaben für den Import. Diese werden beim ersten Start automatisch abgefüllt und können nachträglich im Config - Json geändert werden.

**Artikel**

Parameter|Typ |Bemerkung
---------|----|----------
| Ertragskonto  | int  | Standardertragskonto Artikel  |
| Steuercode    | int  | Standardsteuercode Artikel    |


**Dokument**

Parameter|Typ |Bemerkung
---------|----|----------
| Dokumenttyp  | string  | Standarddokumenttyp  |
| Kondition  | string  | Standardkondition  |
| Lieferart  | string  | Standardlieferart  |


## Installation

Das Tool besteht aus einer einzigen, ausführbaren Datei und **muss entsprechend nicht installiert sondern einfach ausgeführt werden.**


## Download

Die aktuellste Version kann jeweils von uns bezogen werden. Kontaktieren Sie uns doch für Details.

